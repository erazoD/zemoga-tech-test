import VueRouter from 'vue-router';
import Home from './components/Home.vue';
import Trials from './components/Trials.vue';
import Works from './components/Works.vue';
import TermsAndConditions from './components/TermsAndConditions.vue';

let routes = [
  {path:'/', name:'', component:Home},
  {path:'/past-trials', name:'trials' ,component:Trials},
  {path:'/how-it-works', name:'works' ,component:Works},
  {path:'/terms-and-conditions', name:'terms', component:TermsAndConditions}
]

export default new VueRouter({
  mode: 'history',
  hash: false,
  routes});
