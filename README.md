## Zemoga tech test

Hi, this is my technical aptitude test for Zemoga. You'll find the requirements
stablished in the test, except the ones for NodeJS, (I believe I'm not applying for that one)
I hope it is to your liking.
---

## This is a Vue project

The project was made with the Vue Framework, the styling was made with Bulma and Sass and the logic was made with vanilla javascript like it was stated on the test.

Quick install guide for the project (if needed):
1. To set it up first clone the project from the repository
2. Access the project through the command prompt and run "npm install" to install the necessary packages for it to work.
3. Then just run npm run serve.
---

## Branches
The project is divided into three branches: "master", "logicBranch" and "styleBranch" as per the test requirement. Some of the logic was done on the styleBranch due to lack of time. I'm sorry about that. The whole project can be found on the "master" branch.
